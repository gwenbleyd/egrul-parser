import requests, json, os, datetime, traceback, bs4, webbrowser, pandas as pd
from time import sleep

df = pd.read_excel('inn.xlsx', dtype={0: 'string'})
querys = df['ИНН'].tolist()
col = ['ИНН', 'ОГРН', 'КПП', 'Адресс', 'Короткое имя больницы', 'Полное имя больницы', 'Должность', 'ФИО']
df_finaly = pd.DataFrame(columns=col)
s = requests.Session()
n = 0

for item in querys:
    query = querys[n]
    r = s.get("https://egrul.nalog.ru/index.html",
              headers={
                  "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
                  "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
              }
              )

    req = requests.Request(
        'POST',
        'https://egrul.nalog.ru/',
        data=b'vyp3CaptchaToken=&page=&query=' + query.encode() + b'&region=&PreventChromeAutocomplete=',
        headers={
            "Host": "egrul.nalog.ru",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
            "Accept-Encoding": "gzip, deflate, br",
            "Referer": "https://egrul.nalog.ru/index.html",
            "Content-Type": "application/x-www-form-urlencoded",
            "X-Requested-With": "XMLHttpRequest"
        }
    )

    r = s.prepare_request(req)
    r = s.send(r)
    item = json.loads(r.text)
    try:
        if item["ERRORS"] != '' and (item["ERRORS"])["captchaSearch"] != '':
            while True:
                r = s.get('https://egrul.nalog.ru/captcha-dialog.html',
                          headers={
                              "Host": "egrul.nalog.ru",
                              "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
                              "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
                              "Referer": "https://egrul.nalog.ru/index.html",
                              "Pragma": "no-cache",
                              "Cache-Control": "no-cache"
                          })
                b = bs4.BeautifulSoup(r.content.decode(), features="lxml").find('div', class_='field-data').find(
                    'img').get('src')
                webbrowser.open('https://egrul.nalog.ru' + b)
                ct = b.split('?a=')[1].split('&')[0]
                captcha = input('Введите капчу: ')

                r = requests.Request(
                    'POST',
                    'https://egrul.nalog.ru/captcha-proc.json',
                    data=b'captcha=' + captcha.encode() + b'&captchaToken=' + ct.encode(),
                    headers={
                        "Host": "egrul.nalog.ru",
                        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
                        "Accept": "application/json, text/javascript, */*; q=0.01",
                        "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
                        "Accept-Encoding": "gzip, deflate",
                        "Referer": "https://egrul.nalog.ru/index.html"
                    }
                )

                r = s.prepare_request(req)
                r = s.send(r)
                print('captcha r', r.text)
                item = json.loads(r.text)

                try:
                    tr = False
                    if item["ERRORS"] != '':
                        tr = True
                except Exception as e:
                    pass
                if tr is False:
                    break
    except Exception as e:
        pass
    t = json.loads(r.text)['t']

    sleep(0.5)

    r = s.get("https://egrul.nalog.ru/search-result/" + str(t),
              headers={
                  "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0",
                  "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
                  "Referer": "https://egrul.nalog.ru/index.html"
              }
              )

    jsn = json.loads(r.text)

    item = (jsn["rows"])[0]
    inn = str(item['i'])
    ogrn = str(item['o'])
    kpp = str(item['p'])
    adress = item['a']
    short_name = item['c']
    full_name = item['n']
    position = item['g'].split(':')[0]
    name_dir = item['g'].split(':')[1]

    data = {'ИНН': inn, 'ОГРН': ogrn, 'КПП': kpp, 'Адресс': adress, 'Короткое имя больницы': short_name,
            'Полное имя больницы': full_name, 'Должность': position, 'ФИО': name_dir}
    df_finaly = df_finaly._append(data, ignore_index=True)
    n += 1
    sleep(5)

df_finaly.to_excel('Список больницы с врачами.xlsx', index=False)
